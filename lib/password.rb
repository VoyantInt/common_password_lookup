# frozen_string_literal: true

class PasswordValidator
  attr_reader :file, :logger

  def initialize(file, logger: Logger.new(STDOUT))
    @file = file
    @logger = logger
  end

  # @return [Hash]
  def common_passwords
    @common_passwords ||= begin
      file.each.with_object({}) do |row, hsh|
        password = row.strip
        next if password.nil? || password.empty?

        key = password[0].to_sym
        (hsh[key] ||= []) << password
      end
    end
  end

  # @param password [String]
  # @return [String]
  def common?(password)
    key = password[0].to_sym
    common_passwords[key].include?(password)
  rescue StandardError => e
    logger.debug('Password parsing error', message: e.message)

    false
  end
end
