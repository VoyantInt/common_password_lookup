#!/usr/bin/env ruby

# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'

# require 'pry'
require 'logger'
require 'optparse'

Dir['./lib/**/*.rb'].sort.each { |file| require file }

logger = Logger.new(STDOUT)
logger.level = ENV.fetch('LOG_LEVEL') { :debug }.to_sym

FILE_LOCATION = 'vendor/data/common_passwords.txt'
COMMON = 'Common password'
UNCOMMON = 'Not common password'

# Handle exit
at_exit do
  # logger.debug { 'Password validation completed.' }
end

attemped_password = ARGV[0]
raise ArgumentError, 'No password provided' if attemped_password.nil? || attemped_password.empty?

# Output to STDOUT
begin
  file = File.open(FILE_LOCATION)
  validator = PasswordValidator.new(file, logger: logger)
  output = validator.common?(attemped_password) ? COMMON : UNCOMMON

  $stdout.puts(output)
rescue StandardError => e
  logger.error(error: 'An unexpected error occurred', message: e.message, backtrace: e.backtrace)
end
